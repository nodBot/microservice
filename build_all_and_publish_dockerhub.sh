#!/bin/bash

cd shopfront
mvn clean install
if docker build -t officeboy/shopfront . ; then
  docker push officeboy/shopfront
fi
cd ..

cd productcatalogue
mvn clean install
if docker build -t officeboy/productcatalogue . ; then
  docker push officeboy/productcatalogue
fi
cd ..

cd stockmanager
mvn clean install
if docker build -t officeboy/stockmanager . ; then
  docker push officeboy/stockmanager
fi
cd ..
